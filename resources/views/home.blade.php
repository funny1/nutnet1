@extends('layouts.app')

@section('title-block')Пластинки@endsection

@section('content')
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h1 class="display-4">Пластинки</h1>
        <p class="lead">Здесь вы можете посмотреть список своих пластинок</p>
        @include('inc.messages')
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>Название</th>
                <th>Жанр</th>
                <th>Время</th>
                <th></th>
                <th></th>

            </tr>
            </thead>
            <tbody>

                @foreach ($results as $result)

                    <tr>
                        <td>{{$result->name}}</td>
                        <td>{{$result->genre}}</td>
                        <td>{{$result->time}}</td>
                        <td><a class="btn btn-success" href="{{ route('edit-plate', $result->id) }}">Редактировать</a></td>
                        <td><a class="btn btn-danger" href="{{ route('delete-plate', $result->id) }}">Удалить</a></td>
                    </tr>

                @endforeach

            </tbody>
        </table>
        {{$results->links()}}
    </div>
@endsection
