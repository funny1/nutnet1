@extends('layouts.app')

@section('title-block')Редактирование пластинки@endsection

@section('content')

    <div class="col-md-8 order-md-1">
        @include('inc.messages')
        <h4 class="mb-3">Редактирование пластинки</h4>
        <form action="{{ route('editing-completed', $result->id) }}" method="post">
            @csrf

            <div class="mb-3">
                <label for="name">Наименование</label>
                <div class="input-group">
                    <input type="text" class="form-control" name="name" id="name" required="" value="{{$result->name}}">
                </div>
            </div>

            <div class="mb-3">
                <label for="genre">Жанр</label>
                <input type="text" class="form-control" name="genre" id="genre" value="{{$result->genre}}">
            </div>

            <div class="mb-3">
                <label for="time">Время</label>
                <input type="text" class="form-control" name="time" id="time" value="{{$result->time}}">
            </div>

            <button class="btn btn-success" type="submit">Сохранить</button>
            <a class="btn btn-success" href="{{ route('home') }}">Вернуться к списку</a>
        </form>
    </div>

@endsection
