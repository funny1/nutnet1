<?php

Route::get('/', 'HomeController@records')->name('home');

Route::get('/edit/{id}', 'HomeController@oneRecord')->name('edit-plate');

Route::post('/edit/{id}', 'HomeController@oneRecordUpdate')->name('editing-completed');

Route::get('/{id}', 'HomeController@deleteRecord')->name('delete-plate');
