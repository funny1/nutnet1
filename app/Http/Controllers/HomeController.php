<?php

namespace App\Http\Controllers;

use App\Models\Record;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function records(){
        return view('home', ['results'=>Record::paginate(10)]);
    }

    public function oneRecord($id){
        return view('edit', ['result'=>Record::find($id)]);
    }

    public function oneRecordUpdate(Request $req, $id){

        $record = Record::find($id);
        $record->name = $req->input('name');
        $record->genre = $req->input('genre');
        $record->time = $req->input('time');

        $record->save();

        return redirect()->route('edit-plate', $id)->with('success', 'Информация сохранена');

    }

    public function deleteRecord($id){

        Record::find($id)->delete();

        return redirect()->route('home')->with('success', 'Пластинка успешно удалена');
    }
}
