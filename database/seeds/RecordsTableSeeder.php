<?php

use Illuminate\Database\Seeder;

class RecordsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($x=0; $x<20; $x++) {

            DB::table('records')->insert([
                'name' => Str::random(50),
                'genre' => Str::random(20) . '@gmail.com',
                'time' => random_int(1, 90),
            ]);

        }
    }
}
