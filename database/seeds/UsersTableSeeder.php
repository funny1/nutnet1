<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($x=0; $x<5; $x++) {

            DB::table('users')->insert([
                'name' => Str::random(15),
                'email' => Str::random(15) . '@gmail.com',
                'password' => bcrypt('secret'),
            ]);

        }
    }
}
